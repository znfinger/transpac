#!/bin/python
import sys
from transpac.io.FastaIO import FastaReader, FastaWriter
from transpac.io.RepeatMaskerIO import parseRepeatMasker

def create_key(inputs):
    fafn = inputs[2]
    keyfn = inputs[3]
    outfafn = inputs[4]
    keyfh = open(keyfn, 'w')
    with FastaWriter(outfafn) as writer:
        for i,record in enumerate(FastaReader(fafn)):
            writer.writeRecord("%s" % i,record.sequence)
            keyfh.write("%s\t%s\n" % (i,record.name))
    keyfh.close()

def load_keys(keyfn):
    keys = {}
    with open(keyfn, 'r') as fh:
        for line in fh:
            line = line.rstrip().split('\t')
            keys[line[0]] = line[1]
    return keys

def use_key(inputs):
    rmfn = inputs[2]
    keyfn = inputs[3]
    outrmfn = inputs[4]
    keys = load_keys(keyfn)
    outrmfh = open(outrmfn,'w')
    outrmfh.write("None\nNone\nNone\n")
    for record in parseRepeatMasker(rmfn):
        out_record = [record.score,record.div, record.dels,
                      record.ins,keys[record.query_id],
                      record.query_start, record.query_end,"(%s)" % record.query_remaining,
                      record.strand,record.target_id,record.repeat_class,
                      record.target_start,record.target_end,"(%s)" % record.target_remaining,
                      record.query_id]
        out_record = map(str,out_record)
        outrmfh.write("%s\n" % "\t".join(out_record))
    outrmfh.close()

if __name__ == '__main__':
    if len(sys.argv) != 5:
        sys.exit("Use right commands")
    if sys.argv[1] not in ["create_key","use_key"]:
        sys.exit("Use right commands (create_key or use_key)")
    if sys.argv[1] == "create_key":
        create_key(sys.argv)
    if sys.argv[1] == "use_key":
        use_key(sys.argv)
