#!/usr/bin/env python

import sys 
from transpac.io.RepeatMaskerIO import *
from Bio import SeqIO
from difflib import SequenceMatcher

def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()
 
fn2 = SeqIO.parse(open(sys.argv[2]),"fasta")
table = open(sys.argv[3],"r")

tsds = {}
seqs = {}
transeq ={}
 
fw = open("transduction/"+sys.argv[2]+"_ME-transduction.fa","w")

for i in fn2:
	seqs[i.id]=i.seq.tostring()	

for line in table:
	sl = line.strip().split()
	seqid = sl[0]+"_I"
	#if sl[1] < 6:
	#	continue 
	tsd1 = sl[2]
	tsd2 = sl[4]
	tsd_ref = sl[6]
	if similar(tsd1,tsd_ref)>0.85 and similar(tsd2,tsd_ref)>0.85 and len(tsd1)>4 and len(tsd1)<45:
			tsds[seqid] = sl
	

rmes = parseRepeatMasker(sys.argv[1],seqs)
for rme in rmes:
	if rme.isTransduction(rme) is True:
		#print rme
		name = rme.query_id
 		if (name in seqs) and (name in tsds):
			transeq[name] = [">"+name,seqs[name]]
			print rme
		#	print tsds[name]
			fw.write("\n".join(transeq[name])+"\n") 



'''
for repeat_element in rmes:
	if repeat_element.repeat_class == "Retroposon/SVA"  or  repeat_element.repeat_class == "LINE/L1" or  repeat_element.repeat_class == "SINE/Alu":
	if repeat_element.repeat_class == "Retroposon/SVA" and (repeat_element.target_start<100 or repeat_element.target_length - repeat_element.target_end<100):
		print(repeat_element)
	elif repeat_element.repeat_class == "LINE/L1" and repeat_element.target_length - repeat_element.target_end<100:
		print(repeat_element)
	elif repeat_element.repeat_class == "SINE/Alu" and  abs(repeat_element.target_start-repeat_element.target_end) > 0.9*repeat_element.target_length:
		print(repeat_element)
'''






#fill_mei_trs_sats_from_RepeatMasker(rm_fn=sys.argv[1])



