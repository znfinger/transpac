#!/bin/sh 
####USAGE
####bash transpac_assembly.sh query.fa target.fa k Path2RepeatMasker species Reference NThread

mkdir TSDFinder
mkdir TSDFinder/MEI 
mkdir TSDFinder/transduction

query=$1
target=$2
k=$3
Path2RepeatMasker=$4
species=$5  
Ref=$6
NThread=$7

aligner.py $query $target $k --fmt table >> TSDFinder/${query}_TSDFinder_${k}.table
aligner.py $query $target $k --fmt ins_fasta >> TSDFinder/${query}_TSDFinder_${k}_ins.fa

cd TSDFinder
####repeatMasker screening
${Path2RepeatMasker}/RepeatMasker -species $species  ${query}_TSDFinder_${k}_ins.fa

### Calling MEI
MEI_calls.py ${query}_TSDFinder_${k}_ins.fa.out  ${query}_TSDFinder_${k}_ins.fa  ${query}_TSDFinder_${k}.table > MEI/${query}.MEI

### Calling transduciton 
transduction_calls.py ${query}_TSDFinder_${k}_ins.fa.out  ${query}_TSDFinder_${k}_ins.fa  ${query}_TSDFinder_${k}.table > transduction/${query}_transduction_candidates

cd transduction

#module purge
#module load smrtanalysis

##Change Path to smrtanalysis here:
export PATH=/sc/orga/projects/pacbio/modules/smrtanalysis/3.0.0/smrtsuite/smrtcmds/bin:${PATH}

blasr -noSplitSubreads -m 4 -clipping soft  ${query}_TSDFinder_${k}_ME-transduction.fa  $Ref -out  blasr_${query}_TSDFinder_${k}_ins.fa.m4 -nproc $NThread
blasr_full_length.py blasr_${query}_TSDFinder_${k}_ME-transduction.fa.m4 > blasr_${query}_TSDFinder_${k}_ME-transduction.FL



